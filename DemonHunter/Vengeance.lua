
-- Rotation by Neer

local SPELL_SIGIL_OF_FLAME = Spell(204596, 30)
local SPELL_INFERNAL_STRIKE = Spell(189110, 30)
local SPELL_FRACTURE = Spell(263642)
local SPELL_SPIRIT_BOMB = Spell(247454, 40)
local SPELL_SOUL_CLEAVE = Spell(228477)
local SPELL_IMMOLATION_AURA = Spell(178740, 10) -- Offsensive 10 yards
local SPELL_DISRUPT = Spell(183752, 10)
local SPELL_FIERY_BRAND = Spell(204021, 30)
local SPELL_THROW_GLAIVE = Spell(204157, 30)
local SPELL_DEMON_SPIKES = Spell(203720, 10) -- Defensive 10 yards
local SPELL_CONSUME_MAGIC = Spell(278326, 30)
local SPELL_SIGIL_OF_SILENCE = Spell(202137, 30)
local SPELL_SIGIL_OF_MISERY = Spell(207684, 30)
local SPELL_METAMORPHOSIS = Spell(187827, 10)
local SPELL_IMPRISON = Spell(217832, 20)

local Vengeance = {}

function Vengeance.DoCombat(player, target)
    if #player:GetNearbyUnits(12) > 1 then
        Vengeance.AoERotation(player, target)
    else
        Vengeance.SingleRotation(player, target)
    end
end

function Vengeance.AoERotation(player, target)
    Vengeance.SingleRotation(player, target)
end

function Vengeance.SingleRotation(player, target)

	local aura = player:GetAura(203981) -- Aura for fragments

    -- FRAGMENT FUNC
    local fragment = 0
    if aura ~= nil then
        fragment = aura:GetStacks()
    end
    -- FRAGMENT FUNC


    
    -- INTERRUPTS !!!!!!!!!!!!!
	local units = player:GetNearbyEnemyUnits(30)

	for i = 1, #units do
		if units[i]:InCombat() and Vengeance.ShouldInterrupt(units[i]) then
			if SPELL_IMPRISON:CanCast(units[i]) then
				SPELL_IMPRISON:Cast(units[i])
				return
			end
			if not SPELL_IMPRISON:CanCast(units[i]) and SPELL_DISRUPT:CanCast(units[i]) then
				SPELL_DISRUPT:Cast(units[i])
				return
            end
            if not SPELL_IMPRISON:CanCast(units[i]) and not SPELL_DISRUPT:CanCast(units[i]) and SPELL_SIGIL_OF_MISERY:CanCast(units[i]) then
                SPELL_SIGIL_OF_MISERY:CastAoF(units[i]:GetPosition())
                return
            end
            if not SPELL_IMPRISON:CanCast(units[i]) and not SPELL_DISRUPT:CanCast(units[i]) and not SPELL_SIGIL_OF_MISERY:CanCast(units[i]) and SPELL_SIGIL_OF_SILENCE:CanCast(units[i]) then
                SPELL_SIGIL_OF_SILENCE:CastAoF(units[i]:GetPosition())
                return
            end
		end
	end
    -- INTERRUPTS !!!!!!!!!!!!!

    if SPELL_METAMORPHOSIS:CanCast(target) and player:GetHealthPercent() < 25 then
        --SPELL_METAMORPHOSIS:CastAoF(player:GetPosition())
        SPELL_METAMORPHOSIS:Cast(player)
        return
    end

    -- Demon spikes 1st charge at 80% hp
    if SPELL_DEMON_SPIKES:CanCast(target) and SPELL_DEMON_SPIKES:GetCharges() > 1 and player:GetHealthPercent() < 80 then 
        SPELL_DEMON_SPIKES:Cast(player)
        return
    end

    -- Demon spikes 2nd charge at 50% hp
    if SPELL_DEMON_SPIKES:CanCast(target) and SPELL_DEMON_SPIKES:GetCharges() == 1 and player:GetHealthPercent() < 50 then 
        SPELL_DEMON_SPIKES:Cast(player)
        return
    end

    if SPELL_SPIRIT_BOMB:CanCast(target) and fragment > 3 and player:GetPain() >= 30 then
        SPELL_SPIRIT_BOMB:Cast(player)
        return
    end

    if SPELL_SOUL_CLEAVE:CanCast(target) and player:GetPain() > 30 and fragment == 0 then
        SPELL_SOUL_CLEAVE:Cast(target)
        return
    end

    if SPELL_SOUL_CLEAVE:CanCast(target) and player:GetPain() > 30 and player:GetHealthPercent() < 50 then
        SPELL_SOUL_CLEAVE:Cast(target)
        return
    end

    if SPELL_IMMOLATION_AURA:CanCast(target) and player:GetDistance(target) < 8 then
        SPELL_IMMOLATION_AURA:Cast(player)
        return
    end

    if SPELL_FIERY_BRAND:CanCast(target) and player:GetHealthPercent() < 90 then
        SPELL_FIERY_BRAND:Cast(target)
        return
    end

    if SPELL_SIGIL_OF_FLAME:CanCast(target) and not target:IsMoving() then
        SPELL_SIGIL_OF_FLAME:CastAoF(target:GetPosition())
        return
    end

    if (SPELL_FRACTURE:CanCast(target) and fragment <= 4 and SPELL_FRACTURE:GetCharges() > 0) or (SPELL_FRACTURE:CanCast(target) and player:GetPain() < 30 and SPELL_FRACTURE:GetCharges() > 0) then
        SPELL_FRACTURE:Cast(target)
        return
    end

    if not player:IsRooted() and SPELL_INFERNAL_STRIKE:CanCast(target) and player:GetDistance(target) < 6 and not player:IsMoving() then
        SPELL_INFERNAL_STRIKE:CastAoF(player:GetPosition())
        return
    end

    if SPELL_SOUL_CLEAVE:CanCast(target) and player:GetPain() > 30 and SPELL_FRACTURE:GetCharges() == 0 and fragment < 3 then
        SPELL_SOUL_CLEAVE:Cast(target)
        return
    end

    if SPELL_SOUL_CLEAVE:CanCast(target) and player:GetPain() == 100 and fragment < 4 then
        SPELL_SOUL_CLEAVE:Cast(target)
        return
    end

    if SPELL_THROW_GLAIVE:CanCast(target) and SPELL_FRACTURE:GetCharges() == 0 and fragment < 3 then
        SPELL_THROW_GLAIVE:Cast(target)
        return
    end
end

function Vengeance.ShouldInterrupt(target)
    if target:IsCasting() or target:IsChanneling() then
      -- get info and check interruptible
    local sprop = target:GetCurrentSpell()
    if sprop ~= nil then
    if sprop:IsInterruptible() then
                return true -- if interruptable
            end
        end
    end
    return false
  end

return Vengeance