
local SPELL_EYE_BEAM = Spell(198013, 20) -- Offsensive 20 yards
local SPELL_DISRUPT = Spell(183752, 10)
local SPELL_DEMONS_BITE = Spell(162243, 8)
local SPELL_DEATH_SWEEP = Spell(210152, 8)
local SPELL_ANNIHILATION = Spell(201427, 8)
local SPELL_BLADE_DANCE = Spell(188499, 8)
local SPELL_CHAOS_STRIKE = Spell(162794, 8)
local SPELL_IMMOLATION_AURA = Spell(258920, 8)
local SPELL_THROW_GLAIVE = Spell(185123, 20)
local SPELL_CHAOS_NOVA = Spell(179057)
local SPELL_IMPRISON = Spell(217832, 20)

local AURA_META = 162264

local Havoc = {}

function Havoc.DoCombat(player, target)
	if #player:GetNearbyUnits(12) > 1 then
		Havoc.AoERotation(player, target)
	else
		Havoc.SingleRotation(player, target)
	end
end

function Havoc.AoERotation(player, target)
	Havoc.SingleRotation(player, target)
end

function Havoc.SingleRotation(player, target)
	
	local pvp = player:GetNearbyEnemyUnits(20)

	for i = 1, #pvp do
		if Havoc.ShouldInterrupt(pvp[i]) then
			if SPELL_IMPRISON:CanCast(pvp[i]) then
				SPELL_IMPRISON:Cast(pvp[i])
				return
			end
			if not SPELL_IMPRISON:CanCast(pvp[i]) and SPELL_DISRUPT:CanCast(pvp[i]) then
				SPELL_DISRUPT:Cast(pvp[i])
				return
			end
		end
	end

	-- INTERRUPTS

	if #player:GetNearbyEnemyUnits(12) > 1 and SPELL_CHAOS_NOVA:CanCast(player) then
		SPELL_CHAOS_NOVA:Cast(player)
		return
	end


	if SPELL_IMMOLATION_AURA:CanCast(target) and player:GetFury() < 30 and #player:GetNearbyEnemyUnits(10) > 1 then
		SPELL_IMMOLATION_AURA:Cast(player)
		return
	end

	if SPELL_DEATH_SWEEP:CanCast(target) and player:GetFury() >= 15 then
		SPELL_DEATH_SWEEP:Cast(target)
		return
	end
	if SPELL_BLADE_DANCE:CanCast(target) and player:GetFury() >= 15 then
		SPELL_BLADE_DANCE:Cast(target)
		return
	end
	if SPELL_ANNIHILATION:CanCast(target) and player:GetFury() >= 40 then
		SPELL_ANNIHILATION:Cast(target)
		return
	end
	if SPELL_CHAOS_STRIKE:CanCast(target) and player:GetFury() >= 40 and not SPELL_BLADE_DANCE:CanCast(target) and not SPELL_EYE_BEAM:CanCast(target) then
		SPELL_CHAOS_STRIKE:Cast(target)
		return
	end
	if SPELL_THROW_GLAIVE:CanCast(target) and #target:GetNearbyEnemyUnits(10) > 1 and not player:HasAura(AURA_META) and player:GetFury() < 40 then
		SPELL_THROW_GLAIVE:Cast(target)
		return
	end
	if SPELL_DEMONS_BITE:CanCast(target) and player:GetFury() < 40 or SPELL_DEMONS_BITE:CanCast(target) and not SPELL_BLADE_DANCE:CanCast(target) and not SPELL_EYE_BEAM:CanCast(target) and not SPELL_CHAOS_STRIKE:CanCast(target) and not SPELL_ANNIHILATION:CanCast(target) and not SPELL_DEATH_SWEEP:CanCast(target) then
		SPELL_DEMONS_BITE:Cast(target)
		return
	end

	if SPELL_EYE_BEAM:CanCast(target) then
		SPELL_EYE_BEAM:Cast(target)
		return
	end

end

function Havoc.ShouldInterrupt(target)
    if target:IsCasting() or target:IsChanneling() then
      -- get info and check interruptible
    local sprop = target:GetCurrentSpell()
    if sprop ~= nil then
    if sprop:IsInterruptible() then
                return true -- if interruptable
            end
        end
    end
    return false
  end

return Havoc