local SPELL_MOONFIRE = Spell(8921)
local SPELL_SUNFIRE = Spell(93402)
local SPELL_STELLAR_FLARE = Spell(202347)
local SPELL_STARSURGE = Spell(78674)
local SPELL_LUNAR_STRIKE = Spell(194153)
local SPELL_SOLAR_WRATH = Spell(190984)
local SPELL_STARFALL = Spell(191034, 40)
local SPELL_BARKSKIN = Spell(22812)
local SPELL_ENTANGLING_ROOTS = Spell(339)
local SPELL_HIBERNATE = Spell(2637)
local SPELL_CELESTIAL_ALIGNMENT = Spell(194223, 100)
local SPELL_INCARNATION = Spell(102560, 100)
local SPELL_INNERVATE = Spell(29166)
local SPELL_SOLAR_BEAM = Spell(78675)
local SPELL_REGROWTH = Spell(8936)
local SPELL_SOOTHE = Spell(2908)
local SPELL_REMOVE_CORRUPTION = Spell(2782)
local SPELL_WARRIOR_OF_ELUNE = Spell(202425, 100)
local SPELL_MOONKINFORM = Spell(24858)
local SPELL_SWIFTMEND = Spell(18562)
local SPELL_REJUVENATION = Spell(774)
local SPELL_FORCE_OF_NATURE = Spell(205636, 40)
local SPELL_NEW_MOON = Spell(274281)
local SPELL_HALF_MOON = Spell(274282)
local SPELL_FULL_MOON = Spell(274283)
local SPELL_BEAR_FORM = Spell(5487)
local SPELL_RENEWAL = Spell(108238, 100)

local RACIAL_BERSERKING = Spell(26297, 100)

local AURA_MOONFIRE = 164812
local AURA_SUNFIRE = 164815
local AURA_LUNAR_EMPOWERMENT = 164547
local AURA_SOLAR_EMPOWERMENT = 164545
local AURA_STELLAR_FLARE = 202347
local AURA_CATFORM = 768
local AURA_BEARFORM = 5487
local AURA_TRAVELFORM = 783
local AURA_OWLKIN_FRENZY = 157228
local AURA_WARRIOR_OF_ELUNE = 202425
local AURA_ENTANGLING_ROOTS = 339

-- SETTINGS :)
local AssistParty = true -- Should we heal tank at low health, innervate healer when mana threshold hit?
local tankAssist = false -- set to false if tank is a slow shit.
local selfHeal = true -- Should heal self out of combat?
local statueMode = false -- Are we statue farming? 
local useCDS = false
-- SETTINGS :)

local Balance = {}

function Balance.DoCombat(player, target)
	
	--DEBUGGING !]] 
	if player:IsDead() or -- Some checks for not doing shit while being stupid.
		player:IsMounted() or
		player:IsCasting() or
		player:IsChanneling() or
		player:HasTerrainSpellActive() or
		player:IsStunned() or
		Balance.IsShapeshifted(player) then
		return
	end

	local GetPlayers = player:GetNearbyEnemyPlayers(40)
	local EnablePVP = false

	for i = 1, #GetPlayers do
		if target ~= nil and target == GetPlayers[i] and not EnablePVP then
		EnablePVP = true
		end
	end

	if player:InCombat() and EnablePVP then
		Balance.PvPRotation(player, target)
	end

	Balance.GeneralRotation(player, target) -- Some general things.

	if not (player:InParty() or player:InRaid()) and not statueMode and player:InCombat() and not EnablePVP then
		Balance.SoloRotation(player, target)
	end

	if (player:InParty() or player:InRaid()) and not EnablePVP and not statueMode then
		Balance.PartyRotation(player, target)
	end

	if statueMode then
		Balance.StatueRotation(player, target)
	end
end

function Balance.GeneralRotation(player, target)
	-- local farligt = player.Debuffs
	local units = player:GetNearbyEnemyUnits(45)
	local isMoonkin = player:HasAura(24858)

	if player:GetHealthPercent() <= 30 and SPELL_SWIFTMEND:CanCast(player) then
		SPELL_SWIFTMEND:Cast(player)
		return
	elseif player:GetHealthPercent() < 50 and SPELL_RENEWAL:CanCast() then
		SPELL_RENEWAL:Cast(player)
		return
	elseif player:GetHealthPercent() <= 30 and SPELL_REGROWTH:CanCast(player) then
		SPELL_REGROWTH:Cast(player)
		return
	elseif player:GetHealthPercent() <= 40 and SPELL_REJUVENATION:CanCast(player) and not player:HasAuraByPlayer(774) then
		SPELL_REJUVENATION:Cast(player)
		return
	elseif player:GetHealthPercent() < 85 and not player:InCombat() and selfHeal and SPELL_REGROWTH:CanCast(player) and not player:IsMoving() then
		SPELL_REGROWTH:Cast(player)
		return
	elseif player:GetHealthPercent() < 65 and SPELL_BARKSKIN:CanCast() then
		SPELL_BARKSKIN:Cast(player)
		return
	end

	for i = 1, #units do
		local auras = units[i]:GetAuras()
		for k = 1, #auras do
			if auras[k]:GetType() == 9 and SPELL_SOOTHE:CanCast(units[i]) then
				SPELL_SOOTHE:Cast(units[i])
			end
		end
	end

	if not isMoonkin and SPELL_MOONKINFORM:CanCast() then
		SPELL_MOONKINFORM:Cast(player)
		return
	end

	--for i = 1, #farligt do
	--	if farligt[i].Timeleft > 4000 and farligt[i].Type == SpellType.Poison or farligt[i].Type == SpellType.Curse then
	--		if SPELL_REMOVE_CORRUPTION:CanCast(player) then
	--			SPELL_REMOVE_CORRUPTION:Cast(player)
	--		end
	--	end
	-- end
end


-- ##BEGIN STATUE ROTATION !!!!!!!!## -- 
function Balance.StatueRotation(player, target)
	local units = player:GetNearbyEnemyUnits(45)
	local AllAreStacked = false
	local hasFrenzy = player:HasAura(AURA_OWLKIN_FRENZY)
	local hasElune = player:HasAura(AURA_WARRIOR_OF_ELUNE)
	local hasLunar = player:HasAura(AURA_LUNAR_EMPOWERMENT)
	local hasSolar = player:HasAura(AURA_SOLAR_EMPOWERMENT)
	local toAttack = nil
	local getStatue = player:GetNearbyUnits(40)
	local enemiesinCombat = 0
	local statueUnit = nil

	for i = 1, #getStatue do
		if getStatue[i].Entry == 61146 and statueUnit == nil then
			statueUP = true
			statueUnit = getStatue[i]
		end
	end

	for i = 1, #units do
		if units[i]:InCombat() then
			enemiesinCombat = enemiesinCombat + 1
		end
	end

	if statueUnit ~= nil and #statueUnit:GetNearbyEnemyUnits(6) >= enemiesinCombat then
		AllAreStacked = true
	end

	if RACIAL_BERSERKING:CanCast() and useCDS then
		RACIAL_BERSERKING:Cast(player)
	end

	if SPELL_WARRIOR_OF_ELUNE:CanCast() and useCDS then
		SPELL_WARRIOR_OF_ELUNE:Cast(player)
	end

	if enemiesinCombat > 4 and SPELL_CELESTIAL_ALIGNMENT:CanCast() and useCDS then
		SPELL_CELESTIAL_ALIGNMENT:Cast(player)
		return
	end

	for i = 1, #units do
		if not AffectedbyCCOrImmune(units[i]) and not units[i]:IsTapDenied() then -- Check combat and cc then dot
			if not units[i]:HasAuraByPlayer(AURA_MOONFIRE) and SPELL_MOONFIRE:CanCast(units[i]) then
				SPELL_MOONFIRE:Cast(units[i])
				return
			end
			if not units[i]:HasAuraByPlayer(AURA_SUNFIRE) and SPELL_SUNFIRE:CanCast(units[i]) and AllAreStacked then
				SPELL_SUNFIRE:Cast(units[i])
				return
			end

			if not units[i]:HasAuraByPlayer(AURA_STELLAR_FLARE) and SPELL_STELLAR_FLARE:CanCast(units[i]) then
				SPELL_STELLAR_FLARE:Cast(units[i])
				return
			end
		end
	end

	if SPELL_STARFALL:CanCast() and player.AstralPower >= 50 and statueUnit ~= nil then
		SPELL_STARFALL:CastAoF(statueUnit.Position)
		return
	end

	toAttack = FindHighestMob(player)
	if toAttack ~= nil and player:IsFacing(toAttack:GetPosition()) then	
		if SPELL_FORCE_OF_NATURE:CanCast(toAttack) then
			SPELL_FORCE_OF_NATURE:CastAoF(toAttack:GetPosition())
			return
		end	
	
		if SPELL_LUNAR_STRIKE:CanCast(toAttack) and (hasElune or hasFrenzy or hasLunar) and player.AstralPower < 60 then
			SPELL_LUNAR_STRIKE:Cast(toAttack)
			return
		end
		
		if hasSolar and SPELL_SOLAR_WRATH:CanCast(toAttack) and player.AstralPower < 60 then
			SPELL_SOLAR_WRATH:Cast(toAttack)
			return
		end

		if SPELL_STARSURGE:CanCast(toAttack) and #toAttack:GetNearbyEnemyUnits(8) < 4 and player.AstralPower >= 50 then
			SPELL_STARSURGE:Cast(toAttack)
			return
		end

		if SPELL_LUNAR_STRIKE:CanCast(toAttack) and #toAttack:GetNearbyEnemyUnits(8) > 1 then
			SPELL_LUNAR_STRIKE:Cast(toAttack)
			return
		end

		if SPELL_SOLAR_WRATH:CanCast(toAttack) then
			SPELL_SOLAR_WRATH:Cast(toAttack)
			return
		end
		-- Spam Moonfire if moving
		if player.IsMoving and SPELL_MOONFIRE:CanCast(toAttack) then 
			SPELL_MOONFIRE:Cast(toAttack)
			return
		end
	end
end

function Balance.PvPRotation(player, target) -- WIP
	local hasFrenzy = player:HasAura(AURA_OWLKIN_FRENZY)
	local hasElune = player:HasAura(AURA_WARRIOR_OF_ELUNE)
	local hasLunar = player:HasAura(AURA_LUNAR_EMPOWERMENT)
	local hasSolar = player:HasAura(AURA_SOLAR_EMPOWERMENT)
	local GetPlayers = player:GetNearbyEnemyPlayers(45)

	for i = 1, #GetPlayers do -- Dispell enrage and solar beam Entangling roots.
		local auras = GetPlayers[i].Auras
		for k = 1, #auras do
			if auras[k].Type == SpellType.Enrage and SPELL_SOOTHE:CanCast(GetPlayers[i]) then
				SPELL_SOOTHE:Cast(GetPlayers[i])
			end
		end
		if GetPlayers[i]:HasAuraByPlayer(AURA_ENTANGLING_ROOTS) and SPELL_SOLAR_BEAM:CanCast(GetPlayers[i]) then
			SPELL_SOLAR_BEAM:Cast(GetPlayers[i])
		end
	end

	if target ~= nil and not AffectedbyCCOrImmune(target) then -- Only continue if we have a target and its not CCD.
		if player:GetHealthPercent() < 25 and SPELL_BEAR_FORM:CanCast() then
			SPELL_BEAR_FORM:Cast(player)
			return
		end

		if SPELL_MOONFIRE:CanCast(target) and not target:HasAuraByPlayer(AURA_MOONFIRE) then
			SPELL_MOONFIRE:Cast(target)
			return
		end

		if SPELL_SUNFIRE:CanCast(target) and not target:HasAuraByPlayer(AURA_SUNFIRE) then
			SPELL_SUNFIRE:Cast(target)
			return
		end

		if SPELL_FULL_MOON:CanCast(target) and player.AstralPower < 60 then
			SPELL_FULL_MOON:Cast(target)
			return
		end
		
		if SPELL_STARSURGE:CanCast(target) then -- Biggest damage dealer, use whenever.
			SPELL_STARSURGE:Cast(target)
			return
		end

		if SPELL_NEW_MOON:CanCast(target) then
			SPELL_NEW_MOON:Cast(target)
			return
		end

		if SPELL_HALF_MOON:CanCast(target) then
			SPELL_HALF_MOON:Cast(target)
			return
		end

		if hasElune or hasFrenzy and SPELL_LUNAR_STRIKE:CanCast(target) then -- If we have 2 instant cast, prioritise lunar strike over all else.
			SPELL_LUNAR_STRIKE:Cast(target)
			return
		end

		if hasLunar and SPELL_LUNAR_STRIKE:CanCast(target) then -- Bigger damage nuke if lunar is up.
			SPELL_LUNAR_STRIKE:Cast(target)
			return
		end
		
		if hasSolar and SPELL_SOLAR_WRATH:CanCast(target) and not player.IsMoving then -- Solar wrath is slightly under Lunar Strike with both buffs up.
			SPELL_SOLAR_WRATH:Cast(target)
			return
		end

		if SPELL_SOLAR_WRATH:CanCast(target) and not player.IsMoving then -- Solar wrath is best to get Astral while no buffs are up.
			SPELL_SOLAR_WRATH:Cast(target)
			return
		end

		if SPELL_MOONFIRE:CanCast(target) and player.IsMoving then -- Moonfire filler for when moving
			SPELL_MOONFIRE:Cast(target)
			return
		end
	end

end

-- ##BEGIN PARTY ROTATION## --
function Balance.PartyRotation(player, target)
	local units = player:GetNearbyEnemyUnits(45)
	local players = player:GetNearbyFriendlyPlayers(100)
	local foundTank = nil
	local foundHealer = nil
	local enemiesinCombat = 0
	local hasFrenzy = player:HasAura(AURA_OWLKIN_FRENZY)
	local hasElune = player:HasAura(AURA_WARRIOR_OF_ELUNE)
	local hasLunar = player:HasAura(AURA_LUNAR_EMPOWERMENT)
	local hasSolar = player:HasAura(AURA_SOLAR_EMPOWERMENT)
	local MinimumHealthToDot = 1100
	local toAttack = nil

	for i = 1, #units do
		if units[i]:InCombat() then
			enemiesinCombat = enemiesinCombat + 1
		end
	end

	if foundTank == nil or foundHealer == nil then
		for i = 1, #players do
			if players[i]:InParty() and players[i]:GroupRole() == Roles.Tank then
				foundTank = players[i]
			elseif players[i]:InParty() and players[i]:GroupRole() == Roles.Healer then
				foundHealer = players[i]
			end
		end
	end
	
	if AssistParty then
		if foundTank ~= nil and foundTank:GetHealthPercent() <= 30 and SPELL_SWIFTMEND:CanCast(foundTank) then
			SPELL_SWIFTMEND:Cast(foundTank)
			return
		elseif foundTank ~= nil and foundTank:GetHealthPercent() <= 30 and SPELL_REGROWTH:CanCast(foundTank) then
			SPELL_REGROWTH:Cast(foundTank)
			return
		elseif foundHealer ~= nil and foundHealer:ManaPercent() <= 75 and foundHealer:InCombat() and SPELL_INNERVATE:CanCast(foundHealer) then
			SPELL_INNERVATE:Cast(foundHealer)
			return
		end
	end

	if RACIAL_BERSERKING:CanCast() and useCDS and player:InCombat() then
		RACIAL_BERSERKING:Cast(player)
	end

	if SPELL_WARRIOR_OF_ELUNE:CanCast() and useCDS and player:InCombat() then
		SPELL_WARRIOR_OF_ELUNE:Cast(player)
	end

	if enemiesinCombat > 4 and SPELL_CELESTIAL_ALIGNMENT:CanCast() and useCDS then
		SPELL_CELESTIAL_ALIGNMENT:Cast(player)
		return
	end

	if enemiesinCombat > 4 and SPELL_INCARNATION:CanCast() and useCDS then
		SPELL_INCARNATION:Cast(player)
		return
	end

	local bestAoe = BestAoETarget(player, 40, 12)
	if bestAoe ~= nil and SPELL_STARFALL:CanCast() and player:GetAstralPower() >= 50 and #bestAoe:GetNearbyEnemyUnits(12) >= 4 then
		SPELL_STARFALL:CastAoF(bestAoe:GetPosition())
		return
	end

	for i = 1, #units do
		if units[i]:InCombat() and not AffectedbyCCOrImmune(units[i]) and not units[i]:IsTapDenied() then -- Check combat and cc then dot
			if not units[i]:HasAuraByPlayer(AURA_MOONFIRE) and SPELL_MOONFIRE:CanCast(units[i]) and units[i]:GetHealth() > MinimumHealthToDot then
				SPELL_MOONFIRE:Cast(units[i])
				return
			end

			if not units[i]:HasAuraByPlayer(AURA_SUNFIRE) and SPELL_SUNFIRE:CanCast(units[i]) and units[i]:GetHealth() > MinimumHealthToDot then
				SPELL_SUNFIRE:Cast(units[i])
				return
			end

			if not units[i]:HasAuraByPlayer(AURA_STELLAR_FLARE) and SPELL_STELLAR_FLARE:CanCast(units[i]) then
				SPELL_STELLAR_FLARE:Cast(units[i])
				return
			end
		end
	end

	if foundTank ~= nil and tankAssist then
		if foundTank.target ~= nil then
			toAttack = foundTank.target
		end
	elseif target ~= nil and target:IsEnemyWithPlayer() and target:InCombat() then
		toAttack = target
	else
		toAttack = FindHighestMob(player)
	end

	if toAttack ~= nil and player:IsFacing(toAttack:GetPosition()) then	
		if SPELL_FORCE_OF_NATURE:CanCast(toAttack) then
			SPELL_FORCE_OF_NATURE:CastAoF(toAttack:GetPosition())
			return
		end	
	
		if SPELL_LUNAR_STRIKE:CanCast(toAttack) and (hasElune or hasFrenzy or hasLunar) and player:GetAstralPower() < 60 then
			SPELL_LUNAR_STRIKE:Cast(toAttack)
			return
		end
		
		if hasSolar and SPELL_SOLAR_WRATH:CanCast(toAttack) and player:GetAstralPower() < 60 then
			SPELL_SOLAR_WRATH:Cast(toAttack)
			return
		end

		if SPELL_STARSURGE:CanCast(toAttack) and #toAttack:GetNearbyEnemyUnits(8) < 4 and player:GetAstralPower() >= 50 then
			SPELL_STARSURGE:Cast(toAttack)
			return
		end

		if SPELL_LUNAR_STRIKE:CanCast(toAttack) and #toAttack:GetNearbyEnemyUnits(8) > 1 then
			SPELL_LUNAR_STRIKE:Cast(toAttack)
			return
		end

		if SPELL_SOLAR_WRATH:CanCast(toAttack) then
			SPELL_SOLAR_WRATH:Cast(toAttack)
			return
		end

		if player:IsMoving() and SPELL_MOONFIRE:CanCast(toAttack) then -- Moving filler
			SPELL_MOONFIRE:Cast(toAttack)
			return
		end
	end
end


-- ##BEGIN SOLO ROTATION## --
function Balance.SoloRotation(player, target)
	local units = player:GetNearbyEnemyUnits(45)
	local enemiesinCombat = 0
	local hasFrenzy = player:HasAura(AURA_OWLKIN_FRENZY)
	local hasElune = player:HasAura(AURA_WARRIOR_OF_ELUNE)
	local hasLunar = player:HasAura(AURA_LUNAR_EMPOWERMENT)
	local hasSolar = player:HasAura(AURA_SOLAR_EMPOWERMENT)
	local MinimumHealthToDot = 1100
	local toAttack = nil

	for i = 1, #units do
		if units[i]:InCombat() then
			enemiesinCombat = enemiesinCombat + 1
		end
	end

	if RACIAL_BERSERKING:CanCast() and useCDS then
		RACIAL_BERSERKING:Cast(player)
	end

	if SPELL_WARRIOR_OF_ELUNE:CanCast() and useCDS then
		SPELL_WARRIOR_OF_ELUNE:Cast(player)
	end

	if enemiesinCombat > 4 and SPELL_CELESTIAL_ALIGNMENT:CanCast() and useCDS then
		SPELL_CELESTIAL_ALIGNMENT:Cast(player)
		return
	end

	if enemiesinCombat > 4 and SPELL_INCARNATION:CanCast() and useCDS then
		SPELL_INCARNATION:Cast(player)
		return
	end

	local bestAoe = BestAoETarget(player, 40, 12)
	if bestAoe ~= nil and SPELL_STARFALL:CanCast() and player:GetAstralPower() >= 50 and #bestAoe:GetNearbyEnemyUnits(12) >= 4 then
		SPELL_STARFALL:CastAoF(bestAoe:GetPosition())
		return
	end

	for i = 1, #units do
		if units[i]:InCombat() and not AffectedbyCCOrImmune(units[i]) and not units[i]:IsTapDenied() then -- Check combat and cc then dot
		--if enemiesinCombat < 3 then -- Dot units untill this amount of mobs are aggroing you.
			if not units[i]:HasAuraByPlayer(AURA_MOONFIRE) and SPELL_MOONFIRE:CanCast(units[i]) and units[i]:GetHealth() > MinimumHealthToDot then
				SPELL_MOONFIRE:Cast(units[i])
				return
			end

			if not units[i]:HasAuraByPlayer(AURA_SUNFIRE) and SPELL_SUNFIRE:CanCast(units[i]) and units[i]:GetHealth() > MinimumHealthToDot then
				SPELL_SUNFIRE:Cast(units[i])
				return
			end

			if not units[i]:HasAuraByPlayer(AURA_STELLAR_FLARE) and SPELL_STELLAR_FLARE:CanCast(units[i]) then
				SPELL_STELLAR_FLARE:Cast(units[i])
				return
			end
		end
	end

	if target ~= nil and target ~= player then
		toAttack = target
	else
		toAttack = FindHighestMob(player)
	end

	if toAttack ~= nil and player:IsFacing(toAttack:GetPosition()) then	
		if SPELL_FORCE_OF_NATURE:CanCast(toAttack) then
			SPELL_FORCE_OF_NATURE:CastAoF(toAttack:GetPosition())
			return
		end	
	
		if SPELL_LUNAR_STRIKE:CanCast(toAttack) and (hasElune or hasFrenzy or hasLunar) and player:GetAstralPower() < 60 then
			SPELL_LUNAR_STRIKE:Cast(toAttack)
			return
		end
		
		if hasSolar and SPELL_SOLAR_WRATH:CanCast(toAttack) and player:GetAstralPower() < 60 then
			SPELL_SOLAR_WRATH:Cast(toAttack)
			return
		end

		if SPELL_STARSURGE:CanCast(toAttack) and #toAttack:GetNearbyEnemyUnits(8) < 4 and player:GetAstralPower() >= 50 then
			SPELL_STARSURGE:Cast(toAttack)
			return
		end

		if SPELL_LUNAR_STRIKE:CanCast(toAttack) and #toAttack:GetNearbyEnemyUnits(8) > 1 then
			SPELL_LUNAR_STRIKE:Cast(toAttack)
			return
		end

		if SPELL_SOLAR_WRATH:CanCast(toAttack) then
			SPELL_SOLAR_WRATH:Cast(toAttack)
			return
		end

		if player:IsMoving() and SPELL_MOONFIRE:CanCast(toAttack) then -- Moving filler
			SPELL_MOONFIRE:Cast(toAttack)
			return
		end
	end
end

function BestAoETarget(player, range, nearRange)
	local units = player:GetNearbyEnemyUnits(range)
	local bestUnit = nil
	local bestNum = 0
	for i = 1, #units do
		if units[i]:InCombat() and not AffectedbyCCOrImmune(units[i]) then
		local nearUnits = units[i]:GetNearbyEnemyUnits(nearRange)
		if #nearUnits > bestNum then
			bestNum = #nearUnits
			bestUnit = units[i]
		end
	end
end
	return bestUnit
end

function FindHighestMob(player)
	local units = player:GetNearbyEnemyUnits(45)
	local highest = nil
	for i = 1, #units do
		if units[i]:InCombat() and highest == nil then
			highest = units[i]
		end
		if highest ~= nil then
		if units[i]:GetHealthPercent() > highest:GetHealthPercent() and units[i]:InCombat() and player:IsFacing(units[i].Position) and not AffectedbyCCOrImmune(units[i]) and not units[i]:IsTapDenied() then
			highest = units[i]
		end
		end
	end

	return highest
end

function Balance.IsShapeshifted(player)
	if player:HasAura(AURA_CATFORM) or player:HasAura(AURA_BEARFORM) or player:HasAura(AURA_TRAVELFORM) then
		return true
	end

	return false
end

function AffectedbyCCOrImmune(unit)
	-- 115078 = Paralysis
	-- 122 = Frost nova
	-- 62115 = Polymorph
	-- 339 = Entangling roots
	-- 102359 = Mass entangle
	-- 209753 = Cyclone
	-- 2637 = Hibernate
	-- 217832 = Imprison
	-- 3355 = Freezing trap
	-- 642 = Bubble
	-- 20066 = Repentance
	-- 105421 = Blinding light
	-- 51514 = Hex
	-- 31224 = Cloak of shadows
	-- 47585 = Dispersion
	-- 260792 = Dust cloud
	local bads = {115078, 122, 62115, 339, 102359, 209753, 2637, 217832, 3355, 642, 20066, 105421, 51514, 31224, 47585, 260792}
	for i = 1, #bads do
		if unit:HasAura(bads[i]) then
			return true
		end
	end
	return false
end

return Balance